# Building with CMake

## Prerequisets

To build and run this example one need to install `cairo` and `cairomm`.
To build and run tests one also need to install Google test.

For Ubuntu:

```sh
sudo apt install libcairo2 libcairo2-dev libcairomm-1.0-1v5 libcairomm-1.0-dev googletest libgtest-dev
```

## Build

This project doesn't require any special command-line flags to build to keep
things simple.

Here are the steps for building in release mode with a single-configuration
generator, like the Unix Makefiles one:

```sh
cmake -S . -B build -D CMAKE_BUILD_TYPE=Release
cmake --build build
```

Here are the steps for building in release mode with a multi-configuration
generator, like the Visual Studio ones:

```sh
cmake -S . -B build
cmake --build build --config Release
```
