# shapes

This is the shapes project.

## Задание

Необходимо привести пример программы или библиотеки на C++, которая выводит на экран различные геометрические фигуры: круг, квадрат, треугольник, прямоугольник и т.п.
Глубина проработки примера (количество фигур, параметры, методы) на Ваше усмотрение.
Программа не обязательно должна запускаться и работать (хотя это будет плюсом).
В задании Вам необходимо продемонстрировать умение использовать ООП.
Просим не пользоваться библиотекой QT при выполнении задания

## Building and installing

See the [BUILDING](BUILDING.md) document.

## Contributing

See the [CONTRIBUTING](CONTRIBUTING.md) document.

## Notes

чего хочется видеть:

- возможность создавать объекты классов геометрических фигур
  - круг (центр + радиус)
  - произвольный полигон из N вершин (набор вершин)
  - наверное другие фигуры как частные случаи полигона рассматривать не будем
- возможность создать модель данных
  - модель - динамический контейнер, содержащий в себе фигуры
  - наверное можно обозвать модель как Canvas
  - создаем в модели все фигуры, которые хотим нарисовать, затем передаем модель в представление -- рендерер?
  - структура хранения для модели -- пара из точки, где расположена фигура, и сама фигура
  - мысль выше сработает, если фигуры определяются жестко, типа круг, квадрат, прямоугольник, трапеция, ведь тогда почти наверняка будем задавать их параметры напрямую -- через сторону(ы) прямоугольника, etc., длины отрезков, которые надо чертить от точки расположения
  - если завать фигуры как набор вершин в абсолютных координатах, то нет большого смысла хрнаить точку расположения. В конструктор круга будем передавать центр. Остальные фигуры существуют сами по себе.
- возможность создавать отображения / рендереры
  - путь будет хотя бы std::cout
  - если успею сделать вариант для cairo, то буду считать себя молодцом
  - надо формализовать интерфейс, который должны предоставлять рендереры
- по SOLID'у и ООП:
  - постараемся сделать упор на single responsibility и open-close принципы
  - single responsibility заключится в том, что попробуем навернуть model-view-controller (MVC) схему, отвязав фигуры от того, как их надо выводить на экран. Фигурам об этом знать ни к чему;
  - open-close больше блеснёт в рендерерах, попробуем сделать так, чтобы добавление, например, cairo, требовало только добавления новых функций, объясняющих, что делать с фигурами для вывода куда там надо внутрь
  - возможно получится применить std::visit, прям напрашивается (но по памяти вроде будет не найс?)
- как вырисовывается делать:
  - полигонам реально хорошо отдавать итераторы на начало и конец коллекции

## Licensing

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org>
