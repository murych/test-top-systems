#include <gtest/gtest.h>

#include "shape/circle2d.h"
#include "shape/polygon2d.h"
#include "shape/vector2d.h"

TEST(ShapesTests, VectorCreation)
{
  const shapes::Vector2D v{ 0.1, 0.4 };
  EXPECT_DOUBLE_EQ(v.x, 0.1);
  EXPECT_DOUBLE_EQ(v.y, 0.4);
}

TEST(ShapeTests, VectorAddition)
{
  shapes::Vector2D v{ 5.0, 6.0 };
  v += { 23, 5 };

  EXPECT_DOUBLE_EQ(v.x, 28.0);
  EXPECT_DOUBLE_EQ(v.y, 11.0);
}

TEST(ShapeTests, CircleCreation)
{
  const shapes::Circle2D c{ { 100, 500 }, 60 };
  EXPECT_DOUBLE_EQ(c.radius, 60.0);
  EXPECT_DOUBLE_EQ(c.center.x, 100);
  EXPECT_DOUBLE_EQ(c.center.y, 500);
}

TEST(ShapeTests, CircleCreationNegativeRadius)
{
  const shapes::Circle2D c{ { 100, 500 }, -60 };
  EXPECT_DOUBLE_EQ(c.radius, 60);
}

TEST(ShapeTests, PolygonCreation)
{
  const shapes::Polygon2D triangle{ { 10, 20 }, { 0, 0 }, { 50, 50 } };

  // vertices are sorted on construction so {10,20} is no more first vertex
  EXPECT_NE(triangle.cbegin()->x, 10.0);
  EXPECT_NE(triangle.cbegin()->y, 20.0);
}
