#ifndef RENDER_STDOUT_H
#define RENDER_STDOUT_H

#include <iostream>
#include <ostream>

namespace render {

struct RenderStdOut
{
  /*!
   * \brief stream
   * \return reference to standart output stream
   */
  [[nodiscard]] std::ostream& stream() const { return std::cout; }
};

} // namespace render

#endif // RENDER_STDOUT_H
