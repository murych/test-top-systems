#include "renderer_cairo.h"

namespace render {

RenderCairoImage::RenderCairoImage(int width, int height, std::string path)
  : surface{ Cairo::ImageSurface::create(Cairo::Format::FORMAT_ARGB32,
                                         width,
                                         height) }
  , cr{ Cairo::Context::create(surface) }
  , file_path{ std::move(path) }
{
}

RenderCairoImage::~RenderCairoImage()
{
  surface->write_to_png(file_path);
}

} // namespace render
