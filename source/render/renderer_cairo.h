#ifndef RENDER_CAIRO_H
#define RENDER_CAIRO_H

#include <cairomm/context.h>
#include <cairomm/refptr.h>
#include <cairomm/surface.h>

namespace render {

class RenderCairoImage
{
public:
  RenderCairoImage(int width, int height, std::string path);
  ~RenderCairoImage();

  /*!
   * \brief ctx
   * \returns shared pointer to Cairo Context object to perform drawing
   * operations
   */
  [[nodiscard]] auto ctx() const { return cr; }

private:
  Cairo::RefPtr<Cairo::Surface> surface;
  Cairo::RefPtr<Cairo::Context> cr;
  std::string file_path;
};

} // namespace render

#endif // RENDER_CAIRO_H
