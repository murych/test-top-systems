//-----------------------------------------------------------------------------
//
//  demonstration of parent inversion: single responsibility principle and
//    open-closed principle satisfied
//
//-----------------------------------------------------------------------------

#include "render/renderer_cairo.h"
#include "render/renderer_stdout.h"
#include "shape/circle2d.h"
#include "shape/polygon2d.h"
#include <cairomm/context.h>
#include <cairomm/surface.h>
#include <cairommconfig.h>
#include <memory>

// -----------------------------------------------------------------------------
// draw shapes to std::cout
//  add functions with new shapes here

/*!
 * \brief draw point to std::cout
 * \param v - 2d vector/point
 * \param renderer - standard output holder (renderer)
 */
void
draw(shapes::Vector2D v, render::RenderStdOut& renderer)
{
  renderer.stream() << '(' << v.x << ", " << v.y << ')' << '\n';
}

/*!
 * \brief draw polygon to std::cout
 * \param p - 2d polygon
 * \param renderer - standard output holder
 */
void
draw(const shapes::Polygon2D& p, render::RenderStdOut& renderer)
{
  renderer.stream() << "<polygon> ";
  std::for_each(p.cbegin(), p.cend(), [&renderer](auto& vertex) {
    ::draw(vertex, renderer);
  });
  renderer.stream() << " </polygon>" << std::endl;
}

/*!
 * \brief draw circle to std::cout
 * \param c - 2d circle
 * \param renderer - stadard output holder
 */
void
draw(const shapes::Circle2D& c, render::RenderStdOut& renderer)
{
  renderer.stream() << "<circle> ";
  ::draw(c.center, renderer);
  renderer.stream() << " RADIUS: " << c.radius << std::endl;
}

// -----------------------------------------------------------------------------
// draw shapes to cairo image
//  add functions with new shapes here

/*!
 * \brief draw point to cairo png
 * \param v - 2d vector/point
 * \param renderer - cairo context holder
 * \warning won't do anything
 */
void
draw(shapes::Vector2D /*v*/, render::RenderCairoImage& /*renderer*/)
{
}

/*!
 * \brief draw 2d polygon to cairo png
 * \param polygon - 2d polygon
 * \param renderer - cairo context holder
 */
void
draw(const shapes::Polygon2D& polygon, render::RenderCairoImage& renderer)
{
  renderer.ctx()->move_to(polygon.cbegin()->x, polygon.cbegin()->y);
  std::for_each(
    std::next(polygon.cbegin(), 1), polygon.cend(), [&renderer](auto& coords) {
      renderer.ctx()->line_to(coords.x, coords.y);
    });
  renderer.ctx()->close_path();
  renderer.ctx()->stroke();
}

/*!
 * \brief draw 2d circle to cairo png
 * \param circle - 2d circle
 * \param renderer - cairo context holder
 */
void
draw(const shapes::Circle2D& circle, render::RenderCairoImage& renderer)
{
  renderer.ctx()->arc(
    circle.center.x, circle.center.y, circle.radius, 0, 2 * M_PI);
  renderer.ctx()->stroke();
}

// -----------------------------------------------------------------------------
// parent inversion

namespace render {

/*!
 * \brief The Drawable class
 * \details
 */
template<class TRenderer>
class Drawable
{
  /*!
   * \brief The IDrawable class
   * \details defines API for drawable shape that can be printed to TRenderer
   */
  struct IDrawable
  {
    virtual ~IDrawable()                 = default;
    virtual void draw_(TRenderer&) const = 0;
    [[nodiscard]] virtual auto copy_() const -> std::unique_ptr<IDrawable> = 0;
  };

  template<typename T>
  struct DrawableObject final : IDrawable
  {
    T data_;

    DrawableObject(T x)
      : data_{ std::move(x) }
    {
    }
    [[nodiscard]] auto copy_() const -> std::unique_ptr<IDrawable> override
    {
      return std::make_unique<DrawableObject>(*this);
    }

    /*!
     * \details delegating drawing shape (data_) with renderer (out) to free
     * function draw
     * \note overload of function draw has to be declared and defined
     * \param out - template renderer
     */
    void draw_(TRenderer& out) const override { ::draw(data_, out); }
  };

  std::unique_ptr<IDrawable> self_;

public:
  template<typename T>
  Drawable(T x)
    : self_{ std::make_unique<DrawableObject<T>>(std::move(x)) }
  {
  }

  // copy ctor, move ctor and assignment
  Drawable(const Drawable& x)
    : self_{ x.self_->copy_() }
  {
  }
  Drawable(Drawable&& x) noexcept = default;
  Drawable& operator=(Drawable x) noexcept
  {
    self_ = std::move(x.self_);
    return *this;
  }

  /*!
   * \brief draw
   * \details this function will call internal draw_ method of DrawableObject
   * \param x -- drawable shape
   * \param out -- refernce to renderer
   */
  friend void draw(const Drawable& x, TRenderer& out) { x.self_->draw_(out); }
};

} // namespace render

// -----------------------------------------------------------------------------
// data models -- containers of drawable objects represeting "frame" to print

using StdOutModel = std::vector<render::Drawable<render::RenderStdOut>>;
void
draw(const StdOutModel& frame, render::RenderStdOut& renderer)
{
  renderer.stream() << "<world>" << '\n';
  for (const auto& object : frame) {
    draw(object, renderer);
  }
  renderer.stream() << '\n' << "</world>" << std::endl;
}

using CairoModel = std::vector<render::Drawable<render::RenderCairoImage>>;
void
draw(const CairoModel& frame, render::RenderCairoImage& renderer)
{
  for (const auto& object : frame) {
    draw(object, renderer);
  }
}

// -----------------------------------------------------------------------------

int
main()
{
  // print some shapes to std::out
  render::RenderStdOut standard_output;
  StdOutModel document;
  document.push_back(shapes::Polygon2D{ { 2, 1 }, { -3, 7 } });
  document.push_back(shapes::Vector2D{ 7, 4 });
  document.push_back(shapes::Circle2D{ { 6, 4 }, 60 });
  document.push_back(shapes::Polygon2D{ { 200, 200 },
                                        { 25, 100 },
                                        { 50, 50 },
                                        { 75, 50 },
                                        { 270, 50 },
                                        { 25, 200 } });
  draw(document, standard_output);

  // print some shapes to image.png in build directory
  render::RenderCairoImage cairo{ 300, 300, "image.png" };
  CairoModel frame;
  frame.push_back(
    shapes::Polygon2D{ { 50, 10 }, { 50, 60 }, { 10, 30 } }); // triangle
  frame.push_back(shapes::Vector2D{ 7, 4 });             // point (won't print)
  frame.push_back(shapes::Circle2D{ { 100, 150 }, 60 }); // circle
  frame.push_back(shapes::Polygon2D{ { 200, 200 },
                                     { 25, 100 },
                                     { 50, 50 },
                                     { 75, 50 },
                                     { 270, 50 },
                                     { 25, 200 } }); // polygon
  draw(frame, cairo);
}
