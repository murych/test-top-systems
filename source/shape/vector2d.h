#ifndef TASK_VECTOR2D_H
#define TASK_VECTOR2D_H

namespace shapes {

/*!
 * \brief The Vector2D class
 * \details represents point/vector in two-dimensional space
 * \note is expected to always be valid
 */
struct Vector2D
{
  double x{ 0.0 }, y{ 0.0 };

  auto operator+=(shapes::Vector2D lhs) -> Vector2D&
  {
    x += lhs.x;
    y += lhs.y;
    return *this;
  }
};

} // namespace task

#endif // TASK_VECTOR2D_H
