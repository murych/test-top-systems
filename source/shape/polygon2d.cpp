#include "polygon2d.h"

#include <algorithm>
#include <cmath>
#include <numeric>

namespace {

/*!
 * \brief The Vector2dSum class
 * \details functor for using with std::accumulate as BinaryOperation
 */
struct Vector2dSum
{
  auto operator()(const shapes::Vector2D& lhs,
                  const shapes::Vector2D& rhs) const -> shapes::Vector2D
  {
    return { lhs.x + rhs.x, lhs.y + rhs.y };
  }
};

/*!
 * \brief find_centroid
 * \param cbegin - iterator pointing to begin of array of verticies
 * \param cend - iterator pointing to end of array of verticies
 * \return calculated geometric center of polygon
 */
[[nodiscard]] auto
find_centroid(const shapes::Polygon2D::CItt& cbegin,
              const shapes::Polygon2D::CItt& cend) -> shapes::Vector2D
{
  const auto size{ static_cast<double>(std::distance(cbegin, cend)) };
  const auto point{ std::accumulate(
    cbegin, cend, shapes::Vector2D{ 0, 0 }, Vector2dSum()) };

  return { point.x / size, point.y / size };
}

}

namespace shapes {

Polygon2D::Polygon2D(std::initializer_list<Vector2D> il)
  : vs{ il }
{
  sortCw(vs.begin(), vs.end());
}

void
Polygon2D::sortCw(const Itt& begin, const Itt& end)
{
  const auto center{ find_centroid(begin, end) };
  std::sort(begin, end, [center](const auto& lhs, const auto& rhs) {
    const auto a1{ std::atan2(lhs.x - center.x, lhs.y - center.y) };
    const auto a2{ std::atan2(rhs.x - center.x, rhs.y - center.y) };
    return a1 > a2;
  });
}

} // namespace shapes
