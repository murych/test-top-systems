#ifndef SHAPES_CIRCLE2D_H
#define SHAPES_CIRCLE2D_H

#include "vector2d.h"
#include <cmath>
#include <utility>

namespace shapes {

/*!
 * \brief The Circle2D class
 * \note won't check for negative radius
 */
struct Circle2D
{
  const Vector2D center;
  const double radius;

  Circle2D(Vector2D c, double r)
    : center{ std::move(c) }
    , radius{ std::fabs(r) }
  {
  }
};

} // namespace shapes

#endif // SHAPES_CIRCLE2D_H
