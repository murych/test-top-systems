#ifndef SHAPES_POLYGON2D_H
#define SHAPES_POLYGON2D_H

#include "vector2d.h"
#include <vector>

namespace shapes {

/*!
 * \brief The Polygon2D class
 * \details can contain various number of verticies
 * \note sorts verticies clockwise on construction
 * \warning won't check for convexity
 */
struct Polygon2D
{
  Polygon2D(std::initializer_list<Vector2D> il);

  using CItt = typename std::vector<Vector2D>::const_iterator;
  [[nodiscard]] auto cbegin() const -> CItt { return vs.cbegin(); }
  [[nodiscard]] auto cend() const -> CItt { return vs.cend(); }

private:
  std::vector<Vector2D> vs;

  using Itt = typename std::vector<Vector2D>::iterator;
  static void sortCw(const Itt& begin, const Itt& end);
};

} // namespace shapes

#endif // SHAPES_POLYGON2D_H
