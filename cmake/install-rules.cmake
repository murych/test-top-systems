install(
    TARGETS shapes_exe
    RUNTIME COMPONENT shapes_Runtime
)

if(PROJECT_IS_TOP_LEVEL)
  include(CPack)
endif()
